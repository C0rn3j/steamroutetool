#!/usr/bin/env python3

import os, json, platform, subprocess
from urllib.request import urlopen
from PyQt5 import QtCore, QtGui, QtWidgets, QtQuick
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtQuick import *
from typing import *

class TableModel(QtCore.QAbstractTableModel):
	def __init__(self, data):
		super(TableModel, self).__init__()
		self._data = data
		self.blocked = initBlocklist(self._data)
		self.dataChanged.connect(lambda a, _, __ : self.blockedChanged(a.row()) if a.column() == len(self._data[0]) else None)

	blocked: List[bool] = []

	def blockedChanged(self, idx: int):
		IP = self._data[idx][1]
		if self.blocked[idx]:
			print("Blocking: " + IP)
			manageIP(IP, interface, "add")
		else:
			print("Allowing: " + IP)
			manageIP(IP, interface, "del")

	def data(self, index: QtCore.QModelIndex, role):
		if index.column() > len(self._data[0]) or index.row()+1 > len(self._data):
			return QtCore.QVariant()
		if role == Qt.CheckStateRole:
			if index.column() == len(self._data[0]):
				return Qt.Checked if self.blocked[index.row()] else QtCore.Qt.Unchecked
		elif role == Qt.DisplayRole:
			if index.column() == len(self._data[0]):
				return QtCore.QVariant()
			# See below for the nested-list data structure.
			# .row() indexes into the outer list,
			# .column() indexes into the sub-list
			return self._data[index.row()][index.column()]
		elif role == Qt.TextAlignmentRole:
			return Qt.AlignHCenter | Qt.AlignVCenter

	def setData(self, index: QtCore.QModelIndex, data, role = Qt.EditRole):
		if role == Qt.CheckStateRole:
			if index.column() == len(self._data[0]):
				self.blocked[index.row()] = False if data == 0 else True
				self.dataChanged.emit(index, index)
		return True

	def flags(self, index: QtCore.QModelIndex):
		return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable

	def rowCount(self, index):
		# The length of the outer list.
		return len(self._data)

	def columnCount(self, index):
		# The following takes the first sub-list, and returns
		# the length (only works if all rows are an equal length)
		return len(self._data[0])+1

	def headerData(self, column: int, orientation: int, role: int):
		headers = ['Name', 'IP', 'Ping', 'Blocked']
		if column > len(headers):
			return QtCore.QVariant()
		if orientation==QtCore.Qt.Horizontal:
			if role == QtCore.Qt.DisplayRole:
				return QtCore.QVariant(headers[column])
		return QtCore.QVariant()

def initBlocklist(data):
	blockedIPs = []
	command = ['ip', 'r', 'show']
	try:
		result = subprocess.run(command, capture_output=True, text=True)
		routes = result.stdout.splitlines()
		print(routes)
		for route in routes:
			blockedIPs.append(route.split(" ")[0])
		print(blockedIPs)
	except:
		print("Can't get routes")
		exit(1)

	stateIndex = 0
	blocked = []
	for state in data:
		IP = data[stateIndex][1]
		if IP in blockedIPs:
			blocked.append(True)
		else:
			blocked.append(False)
		stateIndex += 1
	return blocked

def ping(host):
	param = '-n' if platform.system().lower()=='windows' else '-c'

	# Building the command. Ex: "ping -c 1 google.com"
	command = ['ping', param, '1', host]
	try:
		result = subprocess.run(command, timeout=2, capture_output=True, text=True)
		# Get result in ms,
		resultText = result.stdout.split("time=")[1].split(" ")[0]
		return resultText
	except:
		return -1

def manageIP(ip, interface, action):
	# Action can be 'add' or 'del'
	command = ['ip', 'r', action, ip, 'via', '0.0.0.0', 'dev', interface]
	try:
		result = subprocess.run(command, capture_output=True, text=True)
		resultText = result.stdout
		return resultText
	except:
		return -1

def about():
	print("https://gitlab.com/C0rn3j/steamroutetool")

def main():
	global interface
	# Valve seemingly stopped providing the config file, so we hardcode it, see https://github.com/dfrood/SteamRouteTool/issues/13
	#networkConfigURL = "https://steamcdn-a.akamaihd.net/apps/sdr/network_config.json"
	networkConfigURL = "https://raw.githubusercontent.com/SteamDatabase/SteamTracking/597d81b2a436d260a7ffe3b53eb3b9ed932c2efb/Random/NetworkDatagramConfig.json"
	output = urlopen(networkConfigURL).read()
	networkConfig = output.decode('utf-8')

#	with open('network_config.json', 'r') as outfile:
#		data = json.load(outfile)
	data = json.loads(networkConfig)

	command = ['ip', 'r', 'show', 'default']
	try:
		result = subprocess.run(command, capture_output=True, text=True)
		resultText = result.stdout.split(" ")[4]
		interface = resultText
		print("Using interface " + interface)
	except:
		print("Can't get interface from default route")
		exit(1)

	print( "Revision: " + str(data["revision"]) )

	tableData = []
	for key in data["pops"]:
		if 'relays' in data["pops"][key]:
			if 'desc' in data["pops"][key]:
				name = data["pops"][key]["desc"]
			else:
				name = key
			for server in data["pops"][key]["relays"]:
				serverIP = server['ipv4'].split(":")[0]
				print("({}) = ({})".format(name, serverIP))
#				print( manageIP( serverIP, interface, "del") )
#				print( ping( serverIP ) )
				tableData.append([name, serverIP, "N/A"])
			print('')

	app = QApplication([])
	window = QWidget()
	window.setWindowTitle("Steam Route Tool")
	layout = QGridLayout()
	table = QtWidgets.QTableView()
	model = TableModel(tableData)
	table.setModel(model)
	# About Button
	aboutButton = QPushButton('About')
	aboutButton.clicked.connect( about )

#	                                              row#, col#, rowspan, colspan
	layout.addWidget( table,                      0, 0, 1, 3 )
	layout.addWidget( QPushButton('Clear Rules'), 1, 0, 1, 1 )
	layout.addWidget( aboutButton,                1, 1, 1, 1 )
	layout.addWidget( QPushButton('Ping Routes'), 1, 2, 1, 1 )
	layout.addWidget( QLabel("Rev: " + str(data["revision"]) + " | Applying for interface: " + interface),          3, 0, 1, 1 )
	window.setLayout( layout )
	window.show()
	app.exec_()

if __name__ == "__main__":
	main()
