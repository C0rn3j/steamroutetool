# SteamRouteTool

Linux version of https://github.com/dfrood/SteamRouteTool

Python 3.7+ required.

Thanks @pontaoski for helping out with Qt stuff!

## License

GNU General Public License (GPL) version 3
